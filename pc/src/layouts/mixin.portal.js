import request from "@/layouts/request.portal";
/*定义导航栏需要的数据参数   header & footer*/
export default {
    data() {
        return ({
            request,
            backgroundColor:'rgb(14, 108, 203)',
            isRequest: true,
            title: "校内班车查询"
        })
    },
    mounted() {
        this.getThemeInfo()
    },
    methods:{
        async getThemeInfo(){
            let {data} = await request.get('/v2/theme/themeInfo')
            try{
                //门户不同版本的主题色数据路径不一致
                let subjectList = data.subjectList || data || [];
                this.backgroundColor = subjectList.filter(x => x.selected)[0].skinList.filter(x => x.selected)[0].color || 'rgb(14, 108, 203)'
            } catch (e) {
                this.backgroundColor = 'rgb(14, 108, 203)'
            }
        }
    }
}
